# Log-forwarding

This helm chart is supposed to contain all the necessary components to ensure that we can collect all logs from a given cluster, filter and transform them according to our needs and format them in such a way that they are perceivable by CERN central monitoring elastic search and then finally send them to CERN central monitoring.

To achieve this we will use a combination of 2 components:

* Deployment of a fluentd Daemonset for log collection - handled by [Cluster Logging Operator](https://docs.openshift.com/container-platform/4.11/logging/cluster-logging.html)
* Deployment of a fluentd instance for log aggregation.

The overall goal of this design is to collect all the logs with an _upstream_ fluentd Daemonset, and have it forward them using `Fluentd Forward` to a custom in-cluster instance of fluentd where we centralize our custom logic.
This instance will then work as an aggregator that will be encharged of processing and formating the logs and then send them to CERN Central Monitoring.

The [Cluster Logging Operator](https://github.com/openshift/cluster-logging-operator) is currently only officially available in the [Red Hat Operators catalog](https://docs.openshift.com/container-platform/4.11/operators/understanding/olm-rh-catalogs.html), therefore we built our own version based on the upstream source: <https://gitlab.cern.ch/paas-tools/okd4-deployment/custom-cluster-logging-operator/>

The Log Aggregator it is the other component in our solution, it is deployed using a Statefulset to try and lose as few logs as possible.
The pods of this set will be run fluentd and they will be responsible for collecting all the logs from all different log sources (daemonset, HAProxy syslogs, etc).
The logs then are filtered, formated and then will finally be sent to central monitoring elastic search.

## Endpoints where the logs can be viewed

See the [OKD4-install project documentation](https://okd-internal.docs.cern.ch/operations/accessing-logs/) for a detailed list of central monitoring providers that we use. For instance the provider to access the Openshift logs is <https://es-openshift.cern.ch/>.

## Design of the event pipelines in our custom logging-aggregator fluentd statefulset

The image below aims at giving a broad idea of how our logging pipeline is structured, we will go over it to explain our design.

![](./images/openshift-logggin-pipeline.png)

Logs can origin from multiple sources, in the image we support the following:

- Logs forwarded by a Fluentd DaemonSet using the protocol Fluentd Forward. Once these logs reach the aggregator we will label them with the label `CLUSTERLOGFORWARDING`, this label will route them to our default log pipeline which is ready to process any container log, system log or audit log produced by the cluster.

- HAProxy logs forwarded by HAProxy using the `rsyslog` protocol. Once these logs reach the aggregator we will label them with the label `HAPROXY`, this label will route them to a dedicated pipeline that will process them.

- Application logs extracted from container logs. For some applications (e.g. webeos access/error logs) we want to parse the logs of the application containers (assuming application logs to STDOUT/STDERR) and send them to a specific provider/type in central monitoring. This is achieved by copying the relevant container logs from `CLUSTERLOGFORWARDING` into a separate, application-specific custom pipeline - see [custom pipelines](#adding-custom-pipelines).

Other sources of logs could also be configured we have represented them in the image using a white box with `...`.

Once logs are received and labeled then they are forwarded to the pipeline which matches their label. In our image, we have 3 pipelines (excluding the one with `...`) two system pipelines; our main pipeline `CLUSTERLOGFORWARDING` (where the majority of logs will go through) and the `HAPROXY` pipeline. Then we have the `WEBEOS_LOGS` pipeline as an example of a user configured pipeline, these types of pipelines are not and should not be defined in this project, they instead defined in projects like okd4-install. We can configure pipelines to modify and process logs in many ways.

For instance, the goal of our main pipeline (`CLUSTERLOGFORWARDING`) is to format the logs so that they match CERN Central Monitoring required format. However, we might also want to do some other operations like:

- Filter out some specific logs;
- Copy logs and label them with a different label so the copy gets routed to another pipeline;
- Transform logs coming from a specific namespace;
- etc.

To allow these multiple possibilities throughout this pipeline we divide it into 5 stages:

1. `Pre Format Filters`
2. `Format Filter`
3. `Post Format Filters`
4. `Pre OUTPUT Send Filters`
5. `OUTPUT Send Filter`

Excluding the stage `Format Filter` and `OUTPUT Send Filter` all other stages can be configured dynamically allowing a very high degree of customization.

A good example where this customization is required is the webeos use case, [here](#adding-custom-pipelines) you
can read more about this example.

Once logs reach the `OUTPUT` pipeline they will be sent to central monitoring using [fluentd's `http` output plugin](https://docs.fluentd.org/output/http) (see [MONIT: HTTP source](https://monit-docs.web.cern.ch/logs/http/)).

### Key points to take away

- Our main pipeline is `CLUSTERLOGFORWARDING`;
- All logs have to be formatted at least once to match CERN Central Monit format requirements;
- Central monitoring [requires that timestamps are UTC](https://monit-docs.web.cern.ch/monit-docs/logs/service_logs.html) and indicate seconds from epoch. We typically need to transform timestamps of logs originating from new sources or application logs after being processed by grok;
- We can dynamically add/remove filters in the `CLUSTERLOGFORWARDING` pipeline as well as add/remove pipelines;
- Fluentd `match` expressions consume all the logs that match the expression and they will not continue throughout the pipeline unless we specify it.

### Excluding container logs

Logs coming from particular pods or namespaces (i.e. all pods in a namespace) can be dropped in the logging pipeline (not forwarded to central monitoring or stdout) by adding the label `logging.okd.cern.ch/store-pod-logs=false` on specific pods or on the namespace to exclude all pods in the namespace (other values than `false` have no effect).

## Adding custom filtering

As previously explained in our design we are able to dynamically add filters to the stages `Pre Format Filters`, `Post Format Filter` and `Pre OUTPUT Send Filters` in the `CLUSTERLOGFORWARDING` pipeline. This is done by prefixing the name of the file where the filter is defined in the following matter:

- For `Pre Format Filters` prefix config file with `clusterlogforwarding-filter-pre-`
- For `Post Format Filter` prefix config file with `clusterlogforwarding-filter-post-`
- For `Pre OUTPUT Send Filters` prefix config file with `clusterlogforwarding-output-pre-`

These config files should be defined in the helm value `userConfigFiles` like the example:

```yaml
userConfigFiles:
  clusterlogforwarding-filter-post-filter-bad-projects.conf: |-
    <filter kubernetes.var.log.pods.**>
        @type grep
        <and>
          <exclude>
          key namespace
          pattern ^test-custom-haproxy-router$
          </exclude>
          <exclude>
          key container_name
          pattern ^custom-haproxy-router$
          </exclude>
        </and>
    </filter>
```

**Note**: On the OKD4-install project these config file can be actual files that live under the folder `chart/charts/cluster-logging/fluentd-user-config`

## Adding custom pipelines

For some applications (e.g. webeos access/error logs) we want to parse and process application logs. The general process is:
1. the application's containers write logs to STDOUT/STDERR
2. these logs are collected as part of container logs
3. we use filters (see details below) to copy the application logs from regular container log pipeline into custom application-specific pipelines
4. parse and process application logs in the custom pipeline

In the same way that we can add custom filters we are also able to add custom pipelines by prefixing the name of the config file where they are defined with `custom-pipeline-`. These config files should also be defined in the helm value `userConfigFiles`.

One case were we did this was in the webeos use case where we wanted to process Apache logs differently from other types of logs but only after they were already formated properly.
To acomplish this we defined the following config files in the helm value `userConfigFiles`:

```yaml
userConfigFiles:
  clusterlogforwarding-output-pre-webeos.conf: |-

    # 1. Make two copies of the logs; to do this we added a filter
    # in the stage "Pre OUTPUT Send Filters". We have to make copy of
    # all the logs since we cannot simply filter out the logs that
    # we want and send them to the "WEBEOS_LOGS" pipeline, as given
    # the way fluentd works we would lose all our other logs as a side effect;

    <match kubernetes.var.log.pods.**>
      @type copy

      # 2. The first copy we will label with "WEBEOS_LOGS" which will send it
      # to the "WEBEOS_LOGS" pipeline;

      <store>
        @type relabel
        @label @WEBEOS_LOGS
      </store>

      # 3. The second copy of the logs will be sent to the "OUTPUT Send Filter"
      # stage which will simply relabel them so that they are sent to the
      # "OUTPUT" pipeline;

      <store>
        @type relabel
        @label @OUTPUT
      </store>
    </match>

  custom-pipeline-webeos.conf: |-
    <label @WEBEOS_LOGS>

      # 4. In the `WEBEOS_LOGS` pipeline we will first filter out all the logs
      # excluding the ones coming from the Apache container;

      <filter kubernetes.var.log.pods.**>
        @type grep
        <and>
          <regexp>
            key namespace
            pattern /^webeos$/
          </regexp>
          <regexp>
            key container_name
            pattern /^httpd$/
          </regexp>
        </and>
      </filter>

      # 5. After filtering we will process them using an Apache
      # Grok pattern, in order to spearate access logs from error logs;

      <filter kubernetes.var.log.pods.**>
        @type parser
        key_name raw
        # keep other fields
        reserve_data true
        # do not remove the `raw` field we parsed.
        # it's needed by the webeos logs application
        remove_key_name_field false
        <parse>
          @type grok
          grok_name_key type
          <grok>
            name "webeos-apache-access"
            pattern %{IPORHOST:servername} %{IPORHOST:clientip} %{HTTPD_COMMONLOG}
          </grok>
          <grok>
            name "webeos-apache-error"
            pattern \[%{IPORHOST:servername}\] %{HTTPD_ERRORLOG}
          </grok>
        </parse>
      </filter>

      # 6. Retag the logs accordingly

      <match kubernetes.var.log.pods.**>
        @type rewrite_tag_filter
        <rule>
          key type
          pattern /^webeos-apache-access$/
          tag webeos-apache-access
        </rule>
        <rule>
          key type
          pattern /^webeos-apache-error$/
          tag webeos-apache-error
        </rule>
      </match>

      # 7. We will then format the logs again (first time was in the stage `Format Filter`)
      # so that they end up in a different endpoint from all our other logs;

      # The next two Fluentd filters are used to transform Viaq-formatted events to something
      # that Central Monitoring understands
      # cf. http://monitdocs.web.cern.ch/monitdocs/ingestion/service_logs.html#http
      <filter webeos-apache-access>
          @type record_modifier
          <record>
            producer  "webinfra"
            # timestamps in access logs (look like 27/Nov/2020:10:42:01 +0100) contain
            # already a timezone information so we just need to parse them and then
            # call .utc to match the expectation of central monitoring
            timestamp ${((DateTime.strptime(record['timestamp'],"%d/%b/%Y:%H:%M:%S %Z").to_time.utc).to_f * 1000).to_i.to_s}
          </record>
      </filter>

      <filter webeos-apache-error>
          @type record_modifier
          <record>
            producer  "webinfra"
            # timestamps in error logs (look like Fri Nov 27 10:42:01.022685 2020)
            # do not contain timezone information and implicitly use the httpd
            # container's local time zone. Because the container image is based
            # on CC7, the local time zone is Europe/Zurich
            # (https://gitlab.cern.ch/linuxsupport/cc7-base/-/blob/master/cc7-base-docker.ks#L8).
            # We must do the conversion explicitly from that time zone to UTC before sending logs
            # to central monitoring.
            timestamp ${((TZInfo::Timezone.get('Europe/Zurich').local_to_utc(DateTime.strptime(record['timestamp'],"%a %b %d %H:%M:%S.%N %Y").to_time)).to_f * 1000).to_i.to_s}
          </record>
      </filter>

      # 8. Finally we will relabel them with the label `OUTPUT` which will send
      # them to the `OUTPUT` pipeline.

      @include /fluentd/etc/send-output.conf
    </label>
```

**Notes**:

- On the OKD4-install project these config file can be actual files that live under the folder `chart/charts/cluster-logging/fluentd-user-config`
- When we introduce new fields, it may be necessary to [refresh the index patterns in Kibana](https://www.oreilly.com/library/view/mastering-kibana-6x/9781788831031/3fa725ff-c835-4152-83ce-58444a3aa357.xhtml) so that the new fields are searchable.

## Troubleshooting

Since our logging set up is composed of many different components sometimes it can break very easily if we change the wrong configuration files or add some disruptive new configurations. Here we have some tips on how to figure out what's wrong.

### Printing logs to stdout

We can easily enable printing logs to stdout by deploying the chart with the values:

```yaml
logging-aggregator:
  enabledOutput:
    - stdout
```

### Problems pushing logs to MONIT

Sometimes it happens that the fluentd aggregator writes logs files faster than what we can send to central monitoring.
If this happens there might be a chance that the aggregator will start to fail with buffer-overflow. To fix this we should delete the pod, this will make it so the pod stops receiving logs allowing it to flush and send the logs in buffer the pod will then be terminated and a new one will be spawned to resume the log processing.

### BufferChunkOverflowError

```
error_class=Fluent::Plugin::Buffer::BufferChunkOverflowError  error="a 767657 bytes record (nth: 7) is larger than buffer chunk limit size (262144), a 748626 bytes record (nth: 11) is larger than buffer chunk limit size (262144), a 748452 bytes record (nth: 15) is larger than buffer chunk limit size (262144)"
```

This error is caused by extremely long log lines that cannot fit into a single chunk (256kB, see `values.yaml`).
To avoid this error message and still ingest at least the start of the log line, we truncate such records to 32K chars (see `output-filter-invalid-records.conf`).


### Monitor the fluentd aggregator logs

When debugging problems with the logging infrastruture it's helpfull to do two things:

* 1. Update the number of aggregator replicas to 1; helpful as this allows us to only have to check the logs of one of the replicas;
* 2. Print the pipeline results to STDOUT instead of sending them to Central Monitoring; helpful to allow us to see what it's actually the result of the processing done by our pipelines.

Both these changes can be achieved by updating the Helm values of the deployment with the following Helm values (note that these are already the default Helm values):

```yaml
replicas: 1
logging-aggregator:
  enabledOutput:
    - stdout
```

### Printing logs before they are sent to OUTPUT pipeline

One thing that might be helpful is printing only the problematic logs to STDOUT before they are sent to the `OUTPUT` pipeline. This is helpful to verify that the logs are being formated. Take particular attention to fileds like `timestamp`. This can be done by adding something like the following to the log pipeline:

```
<match myspeciallogs>
  @type stdout
</match>
<match **>
  @type null
</match>
```
