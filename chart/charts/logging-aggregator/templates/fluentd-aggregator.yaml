apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: fluentd-aggregator
spec:
  selector:
    matchLabels:
      app: fluentd-aggregator
{{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicas }}
{{- end }}
  serviceName: fluentd-aggregator
  updateStrategy: {{- toYaml .Values.updateStrategy | nindent 4 }}
  template:
    metadata:
      labels:
        app: fluentd-aggregator
        # Fluentd should not process its own logs to avoid infinite log forwarding loops
        logging.okd.cern.ch/store-pod-logs: "false"
      annotations:
        checksum/config: {{ include "logForwarding.aggregatorConfig" . | sha256sum }}
    spec:
      serviceAccountName: fluentd-aggregator
      nodeSelector:
{{ toYaml .Values.nodeSelector | indent 8 }}
      tolerations:
{{ toYaml .Values.tolerations | indent 8 }}
      containers:
        - name: fluentd-aggregator
          image: {{ .Values.image }}
          imagePullPolicy: {{ .Values.imagePullPolicy | quote }}
          lifecycle:
            preStop:
              exec:
                command:
                # The kubelet will wait for up to the pod's terminationGracePeriod before sending a TERM signal
                - /bin/sh
                - -c
                - |
                  # pod enters Terminating state. New connections to the service won't be
                  # sent to the terminating pod, but it takes a few seconds for all nodes
                  # to update their iptables rules.
                  sleep 5
                  # terminate existing TCP connections with fluentd config reload
                  kill -SIGUSR2 $(pidof fluentd) && sleep 2
                  # flush memory buffers and wait for buffers to be written on disk
                  kill -SIGUSR1 $(pidof fluentd) && sleep 2
                  # wait for buffer files on disk to be all sent to central monitoring
                  until test "$(ls /var/lib/fluentd/*.buffer | wc -l)" -eq "0"; do sleep 2; done

          env:
            - name: FLUENTD_CONF
              value: fluentd.conf
            - name: FLUENTD_OPT
              value: {{ .Values.extraArgs | quote }}
            - name: K8S_HOST_URL
              value: https://kubernetes.default.svc.cluster.local
            - name: FLUENTD_CPU_LIMIT
              valueFrom:
                resourceFieldRef:
                  containerName: fluentd-aggregator
                  divisor: "0"
                  resource: limits.cpu
            - name: FLUENTD_MEMORY_LIMIT
              valueFrom:
                resourceFieldRef:
                  containerName: fluentd-aggregator
                  divisor: "0"
                  resource: limits.memory
            # Environment specific parameters
            - name: MONIT_ENDPOINT
              value: {{ .Values.monit.endpoint | quote }}
            - name: MONIT_PRODUCER
              value: {{ .Values.monit.producer | quote }}
            - name: MONIT_USERNAME
              valueFrom:
                secretKeyRef:
                  name: fluentd-aggregator-credentials
                  key: monit_username
                  optional: true
            - name: MONIT_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: fluentd-aggregator-credentials
                  key: monit_password
                  optional: true
            - name: CLUSTER_NAME
              value: {{ required "A clusterName value must be provided" .Values.clusterName }}
          ports:
            - name: tcp
              containerPort: {{ .Values.port }}
              protocol: TCP
            {{- if .Values.containerPorts }}
            {{- toYaml .Values.containerPorts | nindent 12 }}
            {{- end }}
          {{- if .Values.livenessProbe.enabled }}
          livenessProbe:
            httpGet:
              path: /fluentd.healthcheck?json=%7B%22ping%22%3A+%22pong%22%7D
              port: http
            initialDelaySeconds: {{ .Values.livenessProbe.initialDelaySeconds }}
            periodSeconds: {{ .Values.livenessProbe.periodSeconds }}
            timeoutSeconds: {{ .Values.livenessProbe.timeoutSeconds }}
            successThreshold: {{ .Values.livenessProbe.successThreshold }}
            failureThreshold: {{ .Values.livenessProbe.failureThreshold }}
          {{- end }}
          {{- if .Values.readinessProbe.enabled }}
          readinessProbe:
            httpGet:
              path: /fluentd.healthcheck?json=%7B%22ping%22%3A+%22pong%22%7D
              port: http
            initialDelaySeconds: {{ .Values.readinessProbe.initialDelaySeconds }}
            periodSeconds: {{ .Values.readinessProbe.periodSeconds }}
            timeoutSeconds: {{ .Values.readinessProbe.timeoutSeconds }}
            successThreshold: {{ .Values.readinessProbe.successThreshold }}
            failureThreshold: {{ .Values.readinessProbe.failureThreshold }}
          {{- end }}
          {{- if .Values.resources }}
          resources: {{- toYaml .Values.resources | nindent 12 }}
          {{- end }}
          volumeMounts:
            - name: fluentd-config-system
              mountPath: /fluentd/etc
            - name: fluentd-config-output
              mountPath: /fluentd/output
            - name: fluentd-config-user
              mountPath: /fluentd/user
            - name: fluentd-filebufferstorage
              mountPath: /var/lib/fluentd

      volumes:
        # ConfigMap that will contain all the basic/static fluentd
        # system configuration files
        - name: fluentd-config-system
          configMap:
            name: fluentd-aggregator-system
        # ConfigMap that will contain all the possible outputs of the logging pipeline
        - name: fluentd-config-output
          configMap:
            name: fluentd-aggregator-output
        # ConfigMap that will contain all the fluentd
        # user configuration files
        - name: fluentd-config-user
          configMap:
            name: fluentd-aggregator-user
        # Dir for fluentd to buffer logs that will then
        # be sent to central monitoring
        - name: fluentd-filebufferstorage
        # Originally here we used hostPathDir however this latter proven to not be the best
        # approach given that some buffer files would get corrupted and to remove them we would need
        # to manualy rsh to the container and manually remove them. With emptyDir we can simply remove the pod
        # and we will get a new volume, we also do not lose information if the pod has to restart given that
        # we are using a StatefulSet.
          emptyDir: {}
