{{/*
This fluentd config file will be the end of our logging pipeline
here as of Dec 2020 we support two outputs, stdout mainly used for debugging
and CERN central monitoring. This setup allows us to send logs to both ouputs or
to a single one if we desire so.
*/}}

{{- define "logForwarding.output" }}
  output.conf: |-
    <match **>
      @type copy

{{- if has "stdout" .Values.enabledOutput }}
      <store>
        @type stdout
      </store>
{{- end -}}

{{ if has "centralMonitoring" .Values.enabledOutput }}
      # Buffer files contain one event per line, json-formatted.
      <store>
        # https://docs.fluentd.org/output/http
        @type http
        endpoint "#{ENV['MONIT_ENDPOINT']}"
        json_array true # important for compatibility with MONIT endpoint
        open_timeout 5
        read_timeout 15
        http_method "post"
        content_type "application/json; charset=UTF-8" # important for compatibility with MONIT endpoint

        {{- if .Values.monit.username }}
        <auth>
          method Basic
          # Do not store credentials in the config file,
          # instead retrieve them from env vars
          username "#{ENV['MONIT_USERNAME']}"
          password "#{ENV['MONIT_PASSWORD']}"
        </auth>
        {{- end }}

        # https://docs.fluentd.org/formatter
        # https://docs.fluentd.org/formatter/json
        <format>
          @type json
          add_newline false # required to keep MONIT endpoint happy
        </format>

        # https://docs.fluentd.org/configuration/buffer-section#buffering-parameters
        <buffer>
          @type file
          # fluentd will create "buffer.${chunk_id}.log" and
          # "buffer.${chunk_id}.log.meta" files in this directory
          path /var/lib/fluentd/http-to-monit/

          chunk_limit_size {{.Values.bufferChunkLimit}}
          total_limit_size {{.Values.bufferLimitSize}}
          chunk_full_threshold 0.85
          disable_chunk_backup true

          flush_at_shutdown true
          flush_mode interval
          flush_interval {{.Values.flushInterval}}
          flush_thread_count {{.Values.flushThreadCount}}

          retry_forever true
          retry_max_interval {{.Values.retryMaxInterval}}
          overflow_action {{.Values.overflowAction}}
        </buffer>

      </store>
{{- end }}
    </match>
{{- end -}}
