# Fluentd filter to transform Viaq-formatted events to something that Central Monitoring understands
# cf. http://monitdocs.web.cern.ch/monitdocs/ingestion/service_logs.html#http
#
# Guidelines:
# - expressions in filters MUST NOT yield errors, because processing of the event continues
#   but the transformation is likely to be incomplete, in particular whitelist_keys may not
#   be effective - leaving nested keys that result in invalid content
#   being submitted to central monitoring, causing various issues: https://gitlab.cern.ch/webservices/webframeworks-planning/-/issues/1073
# - use record.dig for nested fields as per https://docs.fluentd.org/filter/record_transformer#use-dig-method-for-nested-field
# - all fields must have string values. Always default to empty string in case field may be missing.
# - if datetime cannot be parsed, return empty string - this will be filtered out by output-filter-invalid-records.conf


# NOTE: Journal logs are currently broken (not available), see https://gitlab.cern.ch/webservices/webframeworks-planning/-/issues/925
# A systemlog event looks like:
# Tags:
# journal.system
# {
#   "_STREAM_ID":"97940d2f773a4c93b0868347329dd50d",
#   "_SYSTEMD_INVOCATION_ID":"a540ee7c5c1c4213b037fc2055024da5",
#   "systemd":{
#     "t":{
#       "BOOT_ID":"017dbafa332940b1838b1ab3807e5ad5",
#       "CAP_EFFECTIVE":"3fffffffff",
#       "CMDLINE":"kubelet --config=/etc/kubernetes/kubelet.conf --bootstrap-kubeconfig=/etc/kubernetes/kubeconfig --kubeconfig=/var/lib/kubelet/kubeconfig --container-runtime=remote --container-runtime-endpoint=/var/run/crio/crio.sock --runtime-cgroups=/system.slice/crio.service --node-labels=node-role.kubernetes.io/master,node.openshift.io/os_id=fedora --minimum-container-ttl-duration=6m0s --cloud-provider= --volume-plugin-dir=/etc/kubernetes/kubelet-plugins/volume/exec --register-with-taints=node-role.kubernetes.io/master=:NoSchedule --pod-infra-container-image=quay.io/openshift/okd-content@sha256:1207114d4db1bdb9431fdcc890b6813e889fdcfba94900396f0ab9ca4f0c5dbd --v=4",
#       "COMM":"kubelet",
#       "EXE":"/usr/bin/kubelet",
#       "GID":"0",
#       "MACHINE_ID":"054fe6032b2c413eb4f64251591684e8",
#       "PID":"801",
#       "SELINUX_CONTEXT":"system_u:system_r:unconfined_service_t:s0",
#       "STREAM_ID":"97940d2f773a4c93b0868347329dd50d",
#       "SYSTEMD_CGROUP":"/system.slice/kubelet.service",
#       "SYSTEMD_INVOCATION_ID":"a540ee7c5c1c4213b037fc2055024da5",
#       "SYSTEMD_SLICE":"system.slice",
#       "SYSTEMD_UNIT":"kubelet.service",
#       "TRANSPORT":"stdout",
#       "UID":"0"
#     },
#     "u":{
#       "SYSLOG_FACILITY":"3",
#       "SYSLOG_IDENTIFIER":"hyperkube"
#     }
#   },
#   "level":"info",
#   "message":"W1027 00:49:41.351720     801 volume_linux.go:49] Setting volume ownership for /var/lib/kubelet/pods/94abc297-73ca-485d-9b54-333c5e45ef37/volumes/kubernetes.io~configmap/client-ca and fsGroup set. If the volume has a lot of files then setting volume ownership could be slow, see https://github.com/kubernetes/kubernetes/issues/69699",
#   "hostname":"clu-less-master-7vmsc",
#   "pipeline_metadata":{
#     "collector":{
#       "ipaddr4":"188.184.30.76",
#       "inputname":"fluent-plugin-systemd",
#       "name":"fluentd",
#       "received_at":"2020-10-27T10:31:28.748024+00:00",
#       "version":"1.7.4 1.6.0"
#     }
#   },
#   "@timestamp":"2020-10-26T23:49:41.351759+00:00",
#   "viaq_index_name":"infra-write",
#   "viaq_msg_id":"ZWY3NjIyNzAtNGJhOC00M2VhLThiOTUtYTkxYjI0NGNmM2Ni",
#   "kubernetes":{}
# }
<filter journal.system>
  @type record_modifier
  <record>
    cluster_name  ${ENV['CLUSTER_NAME'] || 'okd4-cluster' }
    raw           ${record['message'] || ''}
    host          ${record['hostname'] || ''}
    Facility      ${record.dig('systemd', 'u', 'SYSLOG_FACILITY') || ''}
    program       ${record.dig('systemd', 'u', 'SYSLOG_IDENTIFIER') || ''}
    systemd_unit  ${record.dig('systemd', 't', 'SYSTEMD_UNIT') || ''}
    pid           ${record.dig('systemd', 't', 'PID') || ''}
    producer      ${ENV['MONIT_PRODUCER'] || 'openshift' }
    type          ${ENV['CLUSTER_NAME'] || 'okd4-cluster' }-system
    # timestamp is milliseconds since Unix epoch, no decimal part, as string
    # In case the @timestamp is missing or invalid, evaluate to empty string but make sure not to raise an exception.
    timestamp     ${(Time.iso8601(record['@timestamp']).to_f * 1000).to_i.to_s rescue ''}
  </record>
  whitelist_keys cluster_name,level,raw,host,Facility,program,systemd_unit,pid,producer,type,timestamp
</filter>


# Pod log events look like this:
# Note that the `log_type` can either be "infrastructure" or "application"
# Tags:
# kubernetes.var.log.pods.openshift-cern-argocd_argocd-repo-server-7667697bfb-vgfvd_b73156d8-ae45-46ff-86d7-0beec9f7fddf.repo-server.0.log
# kubernetes.var.log.pods.openshift-cern-argocd_argocd-server-f7bcfc695-g9nfl_ccd20856-bc29-48b8-8e29-c6d985b60e14.server.0.log
# kubernetes.var.log.pods.openshift-cloud-credential-operator_cloud-credential-operator-99dfc46d6-w9w46_026d3b8b-645a-4128-92d1-87aa66bcac9c.cloud-credential-operator.0.log
# kubernetes.var.log.pods.openshift-cluster-version_cluster-version-operator-6c8986667d-9hp8p_7c765609-5657-401a-9925-90285dd87af4.cluster-version-operator.0.log
# {
#   "@timestamp": "2023-03-22T09:56:20.569457346+00:00",
#   "message": "I0322 09:56:20.569381       1 reflector.go:536] k8s.io/client-go@v0.24.0/tools/cache/reflector.go:167: Watch close - *v1.ConfigMap total 9 items received",
#   "docker": {
#     "container_id": "9ee7d98cd59a4a901e0006d612adf0abf77c432e61087dd4cc25f7dd2938a1d2"
#   },
#   "kubernetes": {
#     "container_name": "openshift-controller-manager-operator",
#     "namespace_name": "openshift-controller-manager-operator",
#     "pod_name": "openshift-controller-manager-operator-6654dffb46-lwzjc",
#     "container_image": "registry.cern.ch/paas-tools/okd4-deployment/custom-okd-release/okd-content@sha256:f4aaa157d7ebf7e09b371e671137576649736e6cb78a8346ef17ffcbbc3738c9",
#     "container_image_id": "registry.cern.ch/paas-tools/okd4-deployment/custom-okd-release/okd-content@sha256:f4aaa157d7ebf7e09b371e671137576649736e6cb78a8346ef17ffcbbc3738c9",
#     "pod_id": "df6d24ca-5ac6-4b81-b1f8-c8c7305a35bd",
#     "pod_ip": "10.76.0.8",
#     "host": "clu-jack-dev-master-tdbaq",
#     "labels": {
#       "app": "openshift-controller-manager-operator",
#       "pod-template-hash": "6654dffb46"
#     },
#     "master_url": "https://kubernetes.default.svc",
#     "namespace_id": "fe15d4fc-c039-4b5f-a4ef-25370278c840",
#     "namespace_labels": {
#       "kubernetes.io/metadata.name": "openshift-controller-manager-operator",
#       "olm.operatorgroup.uid/53a64b82-4d4b-4a02-b9b3-3dac9feac997": "",
#       "openshift.io/cluster-monitoring": "true"
#     },
#     "flat_labels": [
#       "app=openshift-controller-manager-operator",
#       "pod-template-hash=6654dffb46"
#     ]
#   },
#   "level": "info",
#   "hostname": "clu-jack-dev-master-tdbaq",
#   "pipeline_metadata": {
#     "collector": {
#       "ipaddr4": "188.184.89.68",
#       "inputname": "fluent-plugin-systemd",
#       "name": "fluentd",
#       "received_at": "2023-03-22T09:56:20.570223+00:00",
#       "version": "1.14.6 1.6.0"
#     }
#   },
#   "openshift": {
#     "labels": {
#       "test_label": "clu-jack-dev"
#     }
#   },
#   "viaq_msg_id": "Njc1N2ZhZGItNTMwZC00ZTExLTg3ZWItMjU3YTkyZjExZjU1",
#   "log_type": "infrastructure"
# }
<filter kubernetes.var.log.pods.**>
  @type record_modifier
  <record>
    user_project           ${record.dig('kubernetes', 'namespace_labels', 'okd.cern.ch/user-project') || 'false'}
    namespace_admin_group  ${record.dig('kubernetes', 'namespace_labels', 'lifecycle.webservices.cern.ch/adminGroup') || ''}
    namespace_owner        ${record.dig('kubernetes', 'namespace_labels', 'lifecycle.webservices.cern.ch/owner') || ''}
    # Add drupalsite field to logs based on pod label (if present)
    drupalsite             ${record.dig('kubernetes', 'labels', 'drupalSite') || ''}
    cluster_name           ${ENV['CLUSTER_NAME'] || 'okd4-cluster' }
    raw                    ${record['message'] || ''}
    host                   ${record['hostname'] || ''}
    producer               ${ENV['MONIT_PRODUCER'] || 'openshift' }
    type                   ${ENV['CLUSTER_NAME'] || 'okd4-cluster' }-container
    container_id           ${record.dig('docker', 'container_id') || ''}
    container_name         ${record.dig('kubernetes', 'container_name') || ''}
    container_image        ${record.dig('kubernetes', 'container_image') || ''}
    container_image_id     ${record.dig('kubernetes', 'container_image_id') || ''}
    namespace              ${record.dig('kubernetes', 'namespace_name') || ''}
    namespace_id           ${record.dig('kubernetes', 'namespace_id') || ''}
    pod_name               ${record.dig('kubernetes', 'pod_name') || ''}
    pod_id                 ${record.dig('kubernetes', 'pod_id') || ''}
    # Container logs can be excluded by setting a label on the pod or the namespace to "false"
    # The field is temporary and will be removed later on
    _store_logs           ${record.dig('kubernetes', 'labels', 'logging.okd.cern.ch/store-pod-logs') || record.dig('kubernetes', 'namespace_labels', 'logging.okd.cern.ch/store-pod-logs') || 'true'}
    # timestamp is milliseconds since Unix epoch, no decimal part, as string
    # In case the @timestamp is missing or invalid, evaluate to empty string but make sure not to raise an exception.
    timestamp              ${(Time.iso8601(record['@timestamp']).to_f * 1000).to_i.to_s rescue ''}
  </record>
  # Drop all other fields from the record. We need to specify at least one field name (e.g. timestamp, producer and type, which are mandatory for MONIT),
  # then all other fields declared under `<record>` are also kept following https://github.com/repeatedly/fluent-plugin-record-modifier/issues/24
  whitelist_keys timestamp,producer,type
</filter>

# Drop all logs that have the "_store_logs" field set to "false"
<filter kubernetes.var.log.pods.**>
  @type grep
  <exclude>
    key _store_logs
    pattern /^false$/
  </exclude>
</filter>

# Remove the "_store_logs" fields from matching entries,
# we just need it for routing purposes, but don't want to preserve it.
<filter kubernetes.var.log.pods.**>
  @type record_modifier
  remove_keys _store_logs
</filter>


# audit log events look like this:
# `oc adm node-logs $MASTER_NODE_NAME --path=kube-apiserver/audit.log`
# https://kubernetes.io/docs/reference/config-api/apiserver-audit.v1/#audit-k8s-io-v1-Event
# Tags:
# openshift-audit.log
# k8s-audit.log
# {
#   "kind": "Event",
#   "apiVersion": "audit.k8s.io/v1",
#   "level": "Metadata",
#   "auditID": "99b4ec98-79db-4192-8470-766de3aa4200",
#   "stage": "ResponseComplete",
#   "requestURI": "/api/v1/persistentvolumes/pvc-5078a5ec-4af5-4519-8119-1f259e6fe06c",
#   "verb": "get",
#   "user": {
#     "username": "system:node:standard-avz-b-7sknf",
#     "groups": [
#       "system:nodes",
#       "system:authenticated"
#     ]
#   },
#   "sourceIPs": [
#     "137.138.157.153"
#   ],
#   "userAgent": "kubelet/v1.22.3+fdba464 (linux/amd64) kubernetes/fdba464",
#   "objectRef": {
#     "resource": "persistentvolumes",
#     "name": "pvc-5078a5ec-4af5-4519-8119-1f259e6fe06c",
#     "apiVersion": "v1"
#   },
#   "responseStatus": {
#     "metadata": {
#     },
#     "code": 200
#   },
#   "requestReceivedTimestamp": "2022-04-05T08:32:26.633759Z",
#   "stageTimestamp": "2022-04-05T08:32:26.667055Z",
#   "annotations": {
#     "authorization.k8s.io/decision": "allow",
#     "authorization.k8s.io/reason": ""
#   }
# }
<filter **-audit.log>
  @type record_modifier
  <record>
    cluster_name     ${ENV['CLUSTER_NAME'] || 'okd4-cluster' }
    level            ${record['level'] || ''}
    host             ${record['hostname'] || ''}
    producer         ${ENV['MONIT_PRODUCER'] || 'openshift' }
    type             ${ENV['CLUSTER_NAME'] || 'okd4-cluster' }-audit
    audit_id         ${record['auditID'] || ''}
    request_uri      ${record['requestURI'] || ''}
    source_ip        ${record.dig('sourceIPs', 0) || ''}
    verb             ${record['verb'] || ''}
    user             ${record.dig('user', 'username') || ''}
    user_agent       ${record['userAgent'] || ''}
    response_status  ${record.dig('responseStatus', 'code') || ''}
    # Possible annotations: https://kubernetes.io/docs/reference/labels-annotations-taints/audit-annotations/
    decision         ${record.dig('annotations', 'authorization.k8s.io/decision') || ''}
    reason           ${record.dig('annotations', 'authorization.k8s.io/reason') || ''}
    validation_failure  ${record.dig('annotations', 'validation.policy.admission.k8s.io/validation_failure') || ''}
    resource         ${record.dig('objectRef', 'resource') || ''}
    namespace        ${record.dig('objectRef', 'namespace') || ''}
    name             ${record.dig('objectRef', 'name') || ''}
    apiGroup         ${record.dig('objectRef', 'apiGroup') || ''}

    # timestamp is milliseconds since Unix epoch, no decimal part, as string
    # In case the @timestamp is missing or invalid, evaluate to empty string but make sure not to raise an exception.
    timestamp     ${(Time.iso8601(record['@timestamp']).to_f * 1000).to_i.to_s rescue ''}
  </record>
  whitelist_keys cluster_name,level,host,producer,type,audit_id,request_uri,source_ips,verb,user,response_status,decision,reason,resource,namespace,name,apiGroup,timestamp
</filter>

# Kubernetes events look like this (see also "oc explain events" for descriptions and "oc get events -A -o json" for more examples):
#  {
#    "message": "Successfully assigned openshift-marketplace/community-operators-gs6jh to clu-jack-dev-master-tdbaq",
#    "source": {
#      "component": "default-scheduler"
#    },
#    "firstTimestamp": "2023-10-23T19:03:30Z",
#    "lastTimestamp": "2023-10-23T19:03:30Z",
#    "involvedObject": {
#      "kind": "Pod",
#      "resourceVersion": "336477742",
#      "apiVersion": "v1",
#      "name": "community-operators-gs6jh",
#      "namespace": "openshift-marketplace",
#      "uid": "21043dba-0c6a-4bb2-b249-5c28ded991af"
#    },
#    "metadata": {
#      "uid": "1515473b-9a50-4482-8b0a-51142b3a29a7",
#      "resourceVersion": "336477745",
#      "creationTimestamp": "2023-10-23T19:03:30Z",
#      "name": "community-operators-gs6jh.1790d1ddeb1f8329",
#      "namespace": "openshift-marketplace"
#    },
#    "reportingInstance": "",
#    "reportingComponent": "default-scheduler",
#    "type": "Normal",
#    "reason": "Scheduled",
#    "count": 1
#  }

<filter k8s_events>
  @type record_modifier
  <record>
    # A human-readable description of the status of this operation.
    # We remap message into "raw" so it can be searched in OpenSearch.
    raw                         ${record['message'] || ''}
    # Name of the controller that emitted this Event, e.g. `kubelet`.
    reporting_component         ${record['reportingComponent'] || ''}
    # ID of the controller instance, e.g. `node-xyzf`.
    reporting_instance          ${record['reportingInstance'] || ''}
    # The object that this event is about.
    involved_object_name        ${record.dig('involvedObject', 'name') || ''}
    involved_object_namespace   ${record.dig('involvedObject', 'namespace') || ''}
    involved_object_kind        ${record.dig('involvedObject', 'kind') || ''}
    involved_object_apiversion  ${record.dig('involvedObject', 'apiVersion') || ''}
    involved_object_fieldpath   ${record.dig('involvedObject', 'fieldPath') || ''}
    # The namespace in which this event occured.
    namespace                   ${record.dig('metadata', 'namespace') || ''}
    # Unique name of this event.
    event_name                  ${record.dig('metadata', 'name') || ''}
    # Type of this event (Normal, Warning).
    # NOTE: we need to save the value of record['type'] before we overwrite it further down
    event_type                  ${record['type'] || ''}
    # A short, machine understandable string that gives the reason for the transition into the object's current status.
    reason                      ${record['reason'] || ''}
    # The number of times this event has occurred.
    count                       ${record['count'].to_s || '1'}

    # Standard record metadata
    cluster_name                ${ENV['CLUSTER_NAME'] || 'okd4-cluster' }
    producer                    ${ENV['MONIT_PRODUCER'] || 'openshift' }
    type                        ${ENV['CLUSTER_NAME'] || 'okd4-cluster' }-events
    # Timestamp is milliseconds since Unix epoch, no decimal part, as string
    # In case the @timestamp is missing or invalid, evaluate to empty string but make sure not to raise an exception.
    timestamp     ${(Time.iso8601(record.dig('metadata', 'creationTimestamp')).to_f * 1000).to_i.to_s rescue ''}
  </record>
  whitelist_keys timestamp,producer,type
</filter>
