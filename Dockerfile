# Our custom deployment of fluentd depends on a fluentd plugin which does
# not come installed by default.
# https://hub.docker.com/r/fluent/fluentd/
FROM docker.io/fluent/fluentd:v1.14.2-1.0

# We have to change to USER 0 to be able to install the fluentd packages
# Since the default user doesn't have permissions to do so
USER 0

RUN echo && \
    # Plugin necessary to allow us formating logs to central monit format
    gem install fluent-plugin-record-modifier && \
    # Plugin necessary for us to rewrite log tags
    gem install fluent-plugin-rewrite-tag-filter && \
    # Plugin necessary for us to use the grok parser on httpd logs
    gem install fluent-plugin-grok-parser && \
    rm -rf /usr/lib/ruby/gems/*/cache/*.gem /usr/lib/ruby/gems/2.*/gems/fluentd-*/test

# Same as the upstream image https://hub.docker.com/r/fluent/fluentd/dockerfile
USER fluent
